swagger: '2.0'
info:
  title: event-service
  version: version not set
tags:
  - name: EventService
consumes:
  - application/json
produces:
  - application/json
paths:
  /v1/events:
    get:
      operationId: EventService_ListEvent
      responses:
        '200':
          description: A successful response.
          schema:
            $ref: '#/definitions/event_serviceListEventResponse'
        default:
          description: An unexpected error response.
          schema:
            $ref: '#/definitions/rpcStatus'
      parameters:
        - name: companyId
          in: query
          required: false
          type: string
          format: int64
        - name: pagination.lastId
          in: query
          required: false
          type: string
          format: int64
        - name: pagination.limit
          in: query
          required: false
          type: string
          format: int64
      tags:
        - EventService
    post:
      operationId: EventService_CreateEvent
      responses:
        '200':
          description: A successful response.
          schema:
            $ref: '#/definitions/event_serviceCreateEventResponse'
        default:
          description: An unexpected error response.
          schema:
            $ref: '#/definitions/rpcStatus'
      parameters:
        - name: name
          in: query
          required: true
          type: string
        - name: description
          in: query
          required: true
          type: string
        - name: location
          in: query
          required: true
          type: string
        - name: duration
          in: query
          required: true
          type: string
        - name: isFree
          in: query
          required: true
          type: boolean
        - name: isPrivate
          in: query
          required: true
          type: boolean
        - name: status
          in: query
          required: true
          type: string
          enum:
            - NEW
            - PUBLISH
            - HIDDEN
            - CANCELED
          default: NEW
        - name: photo_urls
          in: query
          type: array
          required: true
          items:
            type: string

      tags:
        - EventService
  /v1/events/{id}:
    patch:
      operationId: EventService_UpdateEvent
      responses:
        '200':
          description: A successful response.
          schema:
            $ref: '#/definitions/event_serviceUpdateEventResponse'
        default:
          description: An unexpected error response.
          schema:
            $ref: '#/definitions/rpcStatus'
      parameters:
        - name: id
          in: path
          required: true
          type: string
          format: int64
        - name: companyId
          in: query
          required: false
          type: string
          format: int64
        - name: name
          in: query
          required: false
          type: string
        - name: description
          in: query
          required: false
          type: string
        - name: location
          in: query
          required: false
          type: string
        - name: time
          in: query
          required: false
          type: string
          format: date-time
        - name: duration
          in: query
          required: false
          type: string
        - name: isFree
          in: query
          required: false
          type: boolean
        - name: isPrivate
          in: query
          required: false
          type: boolean
      tags:
        - EventService
  /v1/tags:
    get:
      operationId: EventService_ListTags
      responses:
        '200':
          description: A successful response
          schema:
            $ref: '#/definitions/event_serviceListTags'
        default:
          description: An unexpected error response.
          schema:
            $ref: '#/definitions/rpcStatus'
      tags:
        - EventService
    post:
      operationId: EventService_CreateTag
      responses:
        '200':
          description: A successful response.
          schema:
            $ref: '#/definitions/event_serviceCreateTagResponse'
        default:
          description: An unexpected error response.
          schema:
            $ref: '#/definitions/rpcStatus'
      parameters:
        - name: name
          in: query
          required: true
          type: string
        - name: type
          in: query
          required: true
          type: integer
      tags:
        - EventService
  /v1/events/{id}/update-status:
    post:
      operationId: EventService_UpdateEventStatus
      responses:
        '200':
          description: A successful response.
          schema:
            $ref: '#/definitions/event_serviceUpdateEventStatusResponse'
        default:
          description: An unexpected error response.
          schema:
            $ref: '#/definitions/rpcStatus'
      parameters:
        - name: id
          in: path
          required: true
          type: string
          format: int64
        - name: status
          in: query
          required: false
          type: string
          enum:
            - NEW
            - PUBLISH
            - HIDDEN
            - CANCELLED
          default: NEW
      tags:
        - EventService
  /v1/events/{id}/tags/{tag_id}:
    post:
      operationId: EventService_AssignTag
      responses:
        '200':
          description: A successful response.
          schema:
            $ref: '#/definitions/event_serviceAssignTag'
        default:
          description: An unexpected error response.
          schema:
            $ref: '#/definitions/rpcStatus'
      parameters:
        - name: id
          in: path
          required: true
          type: string
          format: int64
        - name: tag_id
          in: path
          required: true
          type: string
          format: int64
      tags:
        - EventService
    delete:
      operationId: EventService_DetachTag
      responses:
        '200':
          description: A successful response.
          schema:
            $ref: '#/definitions/event_serviceDetachTag'
        default:
          description: An unexpected error response.
          schema:
            $ref: '#/definitions/rpcStatus'
      parameters:
        - name: id
          in: path
          required: true
          type: string
          format: int64
        - name: tag_id
          in: path
          required: true
          type: string
          format: int64
      tags:
        - EventService

definitions:
  event_serviceCreateEventResponse:
    type: object
    properties:
      id:
        type: string
        format: int64
  event_serviceEvent:
    type: object
    properties:
      id:
        type: string
        format: int64
      companyId:
        type: string
        format: int64
      name:
        type: string
      description:
        type: string
      location:
        type: string
      duration:
        type: string
      isFree:
        type: boolean
      isPrivate:
        type: boolean
      status:
        $ref: '#/definitions/event_serviceEventStatus'
      photos:
        type: array
        items:
          $ref: '#/definitions/event_servicePhoto'
      createdAt:
        type: string
        format: date-time
      updatedAt:
        type: string
        format: date-time
  event_servicePhoto:
    type: object
    properties:
      uuid:
        type: string
        format: uuid
      url:
        type: string
      is_main:
        type: boolean
  event_serviceEventStatus:
    type: string
    enum:
      - NEW
      - PUBLISH
      - HIDDEN
      - CANCELLED
    default: NEW
  event_serviceListEventResponse:
    type: object
    properties:
      events:
        type: array
        items:
          type: object
          $ref: '#/definitions/event_serviceEvent'
      hasNext:
        type: boolean
  event_servicePagination:
    type: object
    properties:
      lastId:
        type: string
        format: int64
      limit:
        type: string
        format: int64
  event_serviceUpdateEventResponse:
    type: object
    properties:
      event:
        $ref: '#/definitions/event_serviceEvent'
  event_serviceUpdateEventStatusResponse:
    type: object
  event_serviceAssignTag:
    type: object
  event_serviceDetachTag:
    type: object
  event_serviceCreateTagResponse:
    type: object
    properties:
      id:
        type: integer
  event_serviceListTags:
    type: object
    properties:
      tags:
        type: array
        items:
          type: object
          $ref: '#/definitions/event_serviceTag'
  event_serviceTag:
    type: object
    properties:
      id:
        type: integer
      name:
        type: string
      type:
        type: integer

  protobufAny:
    type: object
    properties:
      '@type':
        type: string
    additionalProperties: {}
  rpcStatus:
    type: object
    properties:
      code:
        type: integer
        format: int32
      message:
        type: string
      details:
        type: array
        items:
          type: object
          $ref: '#/definitions/protobufAny'
